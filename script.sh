export KUBECONFIG=~/.kube/config
echo "Starting deployments"
kubectl apply -f deployment.yaml
echo "Starting service"
kubectl apply -f service.yaml