FROM node:20-alpine

WORKDIR /src
COPY index.js .
COPY package.json .

RUN "yarn"

EXPOSE 3000
CMD [ "node", "index" ]
